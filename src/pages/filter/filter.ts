import { Component } from '@angular/core';
import { NavController, LoadingController, MenuController, Slides, AlertController, NavParams } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { HelperProvider } from '../../providers/helper/helper';
import { ViewChild } from '@angular/core';
import { OrderPage } from '../order/order';
import { CariPage } from '../cari/cari';

@Component({
  selector: 'page-filter',
  templateUrl: 'filter.html'
})
export class FilterPage {
  @ViewChild(Slides) slides: Slides;
  jasaList = [];
  subKategoriList = [];
  nextLink:String;
  apiUrl:any;
  id_kategori_jasa = null;
  title:String;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public LoadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public Api: ApiProvider,
    public helper: HelperProvider,
    public MenuCtrl : MenuController,
  ) {
  }

  ionViewDidLoad() {
    this.apiUrl = this.Api.apiUrl;
    this.title = this.navParams.get('title');
    this.id_kategori_jasa = this.navParams.get('id_kategori_jasa');
    // this.id_kategori_jasa = '1';
    this.getJasa()
  }

  order(id_jasa){
    //console.log(id_jasa);
    this.navCtrl.push(OrderPage, {id_jasa: id_jasa});
  }

  search(){
    this.navCtrl.push(CariPage);
  }

  getJasa(){
    let param = {
      id_kategori_jasa: this.id_kategori_jasa
    }
    let loader = this.LoadingCtrl.create({
      content : 'Mohon Menunggu...'
    });
    loader.present().then(()=>{
      this.Api.get('jasa', param).then((r)=>{
        this.jasaList= r['data'];
        this.nextLink = r['meta']['pagination']['links']['next'];
        loader.dismiss();
      }).catch((e)=>{
        loader.dismiss();
      });
    });
  }

  ribuan(nStr){
    return this.helper.ribuan(nStr);
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    let a = this.nextLink;
    if(!a){
      infiniteScroll.complete();
      return;
    }
    this.Api.infinite(a).then((r)=>{

      infiniteScroll.complete();
      for(let i in r['data']){
        this.jasaList.push(r['data'][i]);
      }
      console.log(this.jasaList);
      if(r['meta']['pagination']['links']['next']){
        this.nextLink = r['meta']['pagination']['links']['next'];
      }else{
        this.nextLink = null;
      }
      infiniteScroll.complete();
    }).catch((e)=>{
    });
  }
}
