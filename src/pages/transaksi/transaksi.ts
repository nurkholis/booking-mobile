import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Refresher, Events } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { HelperProvider } from '../../providers/helper/helper';
import { DetailPembelianPage } from '../detail-pembelian/detail-pembelian';
import { DetailPenjualanPage } from '../detail-penjualan/detail-penjualan';

@Component({
  selector: 'page-transaksi',
  templateUrl: 'transaksi.html',
})
export class TransaksiPage {
  @ViewChild(Refresher) refresher: Refresher;
  dataList = [];
  nextLink: String;
  apiUrl = localStorage.getItem('apiUrl');
  limit = 10;
  open = false;
  cari: String;
  loading: Boolean;
  id_member = localStorage.getItem('id_member');
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiProvider,
    private helper: HelperProvider,
    public events: Events,
  ) {
  }

  ionViewDidLoad() {
    this.refresher._top = '60px';
    if (this.id_member) {
      this.refresher._beginRefresh();
      this.events.subscribe('app:reload-getTransaksi', () => {
        this.refresher._beginRefresh();
      });
    }
  }

  getData(prm = null) {
    let param: any;
    if (prm != null) {
      param = prm;
    } else {
      param = {
        id_member: localStorage.getItem('id_member'),
        id_pejasa: localStorage.getItem('id_pejasa'),
        limit: this.limit,
        page: 1,
      }
    }
    this.api.get('transaksi', param).then((r) => {
      this.dataList = r['data'];
      this.nextLink = r['meta']['pagination']['links']['next'];
      this.refresher.complete();
    }).catch(() => {
      this.helper.msg('Gagal memuat data')
      this.refresher.complete();
    });
  }

  detail(id_transaksi, id_member) {
    console.log(id_member, this.id_member);
    
    if (id_member == this.id_member) {
      this.navCtrl.push(DetailPembelianPage, { id_transaksi: id_transaksi })
    } else {
      this.navCtrl.push(DetailPenjualanPage, { id_transaksi: id_transaksi });
    }
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    let a = this.nextLink;
    if (!a) {
      infiniteScroll.complete();
      return;
    }
    this.api.infinite(a).then((r) => {
      infiniteScroll.complete();
      for (let i in r['data']) {
        this.dataList.push(r['data'][i]);
      }
      if (r['meta']['pagination']['links']['next']) {
        this.nextLink = r['meta']['pagination']['links']['next'];
      } else {
        this.nextLink = null;
      }
      infiniteScroll.complete();
    }).catch((e) => {
      console.log('error');
    });
  }

  onCancelSearch() {
    this.getData();
  }

  searchByKeyword() {
    this.search();
  }

  search() {
    let param = {
      id_member: localStorage.getItem('id_member'),
      id_pejasa: localStorage.getItem('id_pejasa'),
      cari: this.cari,
    }
    this.getData(param);
  }

  showSearch() {
    this.open = !this.open;
    this.cari = null;
    if (!this.open) {
      this.getData();
    }
  }

  doRefresh(event) {
    this.getData();
  }

}
