import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Events, Refresher } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { HelperProvider } from '../../providers/helper/helper';
import { DetailTokoPage } from '../detail-toko/detail-toko';
import { UlasanFormPage } from '../ulasan-form/ulasan-form';

@Component({
  selector: 'page-detail-pembelian',
  templateUrl: 'detail-pembelian.html',
})
export class DetailPembelianPage {
  
  @ViewChild(Refresher) refresher: Refresher;
  loading: Boolean;
  id_transaksi: any;
  transaksi = [];
  apiUrl = localStorage.getItem('apiUrl');
  pesan = {
    pesan_diterima: null,
    pesan_ditolak: null,
    pesan_selesai: null,
  }
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public events: Events,
    public api: ApiProvider,
    public helper: HelperProvider,
  ) {
  }

  ionViewDidLoad() {
    this.refresher._top = '60px';
    this.id_transaksi = this.navParams.get('id_transaksi');
    // this.id_transaksi = '1';
    this.refresher._beginRefresh();
  }

  lihatToko(){
    let id_pejasa = this.transaksi['member']['id_member'];
    this.navCtrl.push(DetailTokoPage, {id_pejasa: id_pejasa});
  }

  doRefresh(){
    this.getTransaksi(this.id_transaksi);
  }

  getTransaksi(id_transaksi) {
    let param = {
      id_transaksi: id_transaksi
    };
    this.api.get('transaksi', param).then((r) => {
      this.transaksi = r['data'][0];
      this.refresher.complete();
    }).catch((e) => {
      this.refresher.complete();
      this.helper.msg('gagal membuat data transaksi');
    });
  }

  ulasan(id_jasa){
    this.navCtrl.push(UlasanFormPage, {id_jasa: id_jasa});
  }


}
