import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Events, Refresher } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { HelperProvider } from '../../providers/helper/helper';
import { DetailTokoPage } from '../detail-toko/detail-toko';

@Component({
  selector: 'page-detail-penjualan',
  templateUrl: 'detail-penjualan.html',
})
export class DetailPenjualanPage {
  
  @ViewChild(Refresher) refresher: Refresher;
  loading: Boolean;
  id_transaksi: any;
  transaksi = [];
  apiUrl = localStorage.getItem('apiUrl');
  pesan = {
    pesan_diterima: null,
    pesan_ditolak: null,
    pesan_selesai: null,
  }
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public events: Events,
    public api: ApiProvider,
    public helper: HelperProvider,
  ) {
  }
  

  ionViewDidLoad() {
    this.refresher._top = '60px';
    this.id_transaksi = this.navParams.get('id_transaksi');
    // this.id_transaksi = '1';
    this.refresher._beginRefresh();
  }

  lihatToko(){
    let id_pejasa = this.transaksi['pejasa']['id_pejasa'];
    this.navCtrl.push(DetailTokoPage, {id_pejasa: id_pejasa});
  }

  doRefresh(){
    this.getTransaksi(this.id_transaksi);
  }

  getTransaksi(id_transaksi) {
    let param = {
      id_transaksi: id_transaksi
    };
    this.api.get('transaksi', param).then((r) => {
      this.transaksi = r['data'][0];
      this.refresher.complete();
    }).catch((e) => {
      this.refresher.complete();
      this.helper.msg('gagal membuat data transaksi');
    });
  }

  kirim(mode) {
    // validasi pesan dulu
    let param:any;
    if (mode == 'diterima') {
      if (!this.pesan.pesan_diterima) {
        return this.helper.msg('Pesan diterima harus diisi');
      }
      param ={
        pesan_diterima: this.pesan.pesan_diterima,
        _method: 'PUT'
      }
    }
    else if (mode == 'ditolak') {
      if (!this.pesan.pesan_ditolak) {
        return this.helper.msg('Pesan ditolak harus diisi');
      }
      param ={
        pesan_ditolak: this.pesan.pesan_ditolak,
        _method: 'PUT'
      }
    }
    else {
      if (!this.pesan.pesan_selesai) {
        return this.helper.msg('Pesan selesai harus diisi');
      }
      param ={
        pesan_selesai: this.pesan.pesan_selesai,
        _method: 'PUT'
      }
    }
    let l = this.helper.loader();
    l.present().then(() => {
      let header = {
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      }
      this.api.post('transaksi', param, header, this.id_transaksi).then((r) => {
        this.refresher._beginRefresh();
        l.dismiss();
        this.helper.msg('Berhasil mengirim pesan')
      }).catch((e) => {
        l.dismiss();
        this.helper.msg('Gagal mengirim pesan')
      })
    })
  }

}
