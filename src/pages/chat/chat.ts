import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Refresher, Events } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { HelperProvider } from '../../providers/helper/helper';
import { ChatKirimPage } from '../chat-kirim/chat-kirim';

@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {
  @ViewChild(Refresher) refresher: Refresher;
  pesanList = [];
  apiUrl: string;
  id_member = localStorage.getItem('id_member');
  timer = setInterval(()=>{
    this.getPesan();
  }, 5000);
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public events: Events,
    public api: ApiProvider,
    public helper: HelperProvider,
  ) {
  }

  ionViewDidLoad() {
    this.apiUrl = this.api.apiUrl;
    this.refresher._top = '60px';
    this.refresher._beginRefresh();
    this.events.subscribe('app:reload-getPesan', () => {
      this.refresher._beginRefresh();
    });
  }

  ionViewDidLeave() {
    clearInterval(this.timer);
  }

  doRefresh(event) {
    this.getPesan();
  }

  room(id_penerima, nama, foto){
    this.navCtrl.push(ChatKirimPage, {id_penerima: id_penerima, nama: nama, foto:  foto});
  }

  getPesan() {
    let param = {
      id_member: this.id_member,
    }
    this.api.get('chat.room', param).then((r) => {
      this.pesanList = r['data'];
      this.refresher.complete();
    }).catch(() => {
      this.helper.msg('Gagal memuat data')
      this.refresher.complete();
    });
  }

}
