import { Component, ViewChild } from '@angular/core';
import { NavController, MenuController, Slides, AlertController, LoadingController, App, Events, Refresher } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { HelperProvider } from '../../providers/helper/helper';
import { DeveloperPage } from '../developer/developer';
import { OrderPage } from '../order/order';
import { KeranjangPage } from '../keranjang/keranjang';
import { FilterPage } from '../filter/filter';
import { ChatPage } from '../chat/chat';
import { LoginPage } from '../login/login';
import { GeolocationProvider } from '../../providers/geolocation/geolocation';
import { CariPage } from '../cari/cari';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild(Refresher) refresher: Refresher;
  @ViewChild(Slides) slides: Slides;
  jumlahKeranjang: any;
  apiUrl: any;
  webUrl: any;
  loading: Boolean;
  jasa = [];
  KategoriJasa = [];
  id_member = localStorage.getItem('id_member');
  status:string;
  loop = setInterval(()=>{
    this.getStatus();
  }, 5000);
  constructor(
    public navCtrl: NavController,
    public api: ApiProvider,
    public menu: MenuController,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    private helper: HelperProvider,
    private app: App,
    public events: Events,
    public geolocation: GeolocationProvider,
  ) {
  }
  ionViewDidLoad() {
    this.geolocation.activatedGPS();
    this.geolocation.loadLocation();
    this.apiUrl = this.api.apiUrl;
    this.events.subscribe('home:reload-getJumlahKeranjang', () => {
      this.getJumlahKeranjang();
    });
    this.getStatus();
    this.refresher._beginRefresh();
  }
  
  getStatus(){
    this.status = localStorage.getItem('status');
    if( this.status == '1' || !this.status){
      clearInterval(this.loop);
    }else{
      let header = {
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      }
      this.api.get('member.profil', null, header).then((r) => {
        localStorage.setItem('status', r['data']['status']);
      }).catch(() => {
      })
    }
  }

  search(){
    this.navCtrl.push(CariPage);
  }

  doRefresh(event){
    this.getJasa();
    this.getKategoriJasa();
    this.getJumlahKeranjang();
   }

  filter(id_kategori_jasa, nama_kategori_jasa) {
    this.navCtrl.push(FilterPage, { id_kategori_jasa: id_kategori_jasa, title: nama_kategori_jasa });
  }

  getJasa() {
    let param = {
      status: '1',
    }
    this.api.get('jasa', param).then((r) => {
      this.refresher.complete();
      this.jasa = r['data'];
    }).catch(() => {
      this.refresher.complete();
      this.helper.msg('Gagal memuat jasa')
    });
  }

  keranjang() {
    this.app.getRootNav().push(KeranjangPage);
  }

  chat() {
    if(this.id_member){
      this.app.getRootNav().push(ChatPage);
    }else{
      this.navCtrl.push(LoginPage);
    }
  }

  getKategoriJasa() {
    this.api.get('kategori.jasa').then((r) => {
      this.KategoriJasa = r['data'];
    }).catch(() => {
      this.helper.msg('Gagal memuat kategori jasa')
    });
  }

  getJumlahKeranjang() {
    let keranjangList = JSON.parse(localStorage.getItem('keranjang'));
    this.jumlahKeranjang = keranjangList.length;
  }

  order(id_jasa) {
    this.app.getRootNav().push(OrderPage, { id_jasa: id_jasa });
  }

  openDevPage() {
    this.navCtrl.push(DeveloperPage);
  }

}
