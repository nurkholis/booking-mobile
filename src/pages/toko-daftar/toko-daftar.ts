import { Component } from '@angular/core';
import { NavController, NavParams, Platform, ActionSheetController, App, Button } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { ApiProvider } from '../../providers/api/api';
import { DatePicker } from '@ionic-native/date-picker';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-toko-daftar',
  templateUrl: 'toko-daftar.html',
})
export class TokoDaftarPage {
  toko ={
    id_kategori_jasa: null,
    nama_toko: '',
    logo: null,
    ktp: null,
    id_kecamatan: '3526070',
    latitude: '1111',
    longitude: '1111',
    accept: null,
  }
  kategoriList = [];
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public helper: HelperProvider,
    public api: ApiProvider,
    public platform: Platform,
    public datePicker: DatePicker,
    public actionSheetCtrl: ActionSheetController,
    private camera: Camera,
    public app: App,
    ) {
  }

  ionViewDidLoad() {
    this.platform.ready().then(()=>{
      this.getAlamat();
      this.getKategori();
      console.log('plat form ready');
      this.getIdKecamatan('PRAGAAN');
    })
  }

  back(){
    this.app.getRootNav().setRoot(TabsPage);
  }

  getKategori(){
    this.api.get('kategori.jasa').then((r)=>{
      this.kategoriList = r['data'];
    }).catch((e)=>{
      this.helper.msg('Gagal memuat kategori');
    });
  }

  getAlamat(){
    let lat = localStorage.getItem('lat');
    let long = localStorage.getItem('long');
    this.api.infinite('http://maps.googleapis.com/maps/api/geocode/json?latlng='+ lat + ',' + long).catch((r)=>{
      
    }).catch((e)=>{

    })
  }

  daftar(){
    let header = {
      Authorization  : 'Bearer ' + localStorage.getItem('token'),
    }
    let l = this.helper.loader();
    l.present().then(()=>{
      this.api.post('pejasa', this.toko, header).then((r)=>{
        l.dismiss();
        localStorage.setItem('id_pejasa', r['data']['id_pejasa']);
        localStorage.setItem('status_pejasa', '0');
        localStorage.setItem('id_kategori_jasa', r['data']['id_kategori_jasa']);
        this.helper.alertCtrl.create({
          subTitle: 'Berhasil',
          message: 'Selamat pendaftaran berhasil! kami akan segera melakukan verifikasi pada usaha jasa anda. nikmati dulu secangkir teh sembari menunggu',
          buttons: [
            {
              text : 'Beranda',
              handler: ()=>{
                this.app.getRootNav().setRoot(TabsPage);
              }
            }
          ]
        }).present();
      }).catch((e)=>{
        l.dismiss();
        this.helper.alert('Gagal', this.helper.error(e));
      })
    });
  }

  getIdKecamatan(nama_kecamatan){
    let param = {
      nama_kecamatan : nama_kecamatan
    };
    this.api.get('kecamatan', param).then((r)=>{
      if(!r['data'][0]){
        this.helper.msg('Kecamatan tidak ditemukan');
        return; 
      }
    }).catch((e)=>{
      this.helper.msg('Gagal memuat data kecamatan');
    })
  }

  ambilGambar(mode){
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Pilih Sumber Gambar',
      buttons: [
        {
          text: 'Dari Pustaka',
          icon: 'image',
          handler: () => {
            this.ambilPustaka(mode);
          }
        },
        {
          text: 'Dari Kamera',
          icon: 'camera',
          handler: () => {
            this.ambilFoto(mode);
          }
        }, {
          text: 'Batal',
          role: 'cancel'
        }
      ]
    });
      actionSheet.present();
  }

  ambilFoto(mode) {
    const options: CameraOptions = {
      quality: 100,
      targetWidth: 500,
      targetHeight: 500,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: true,
      allowEdit: true,
    }
    this.camera.getPicture(options).then((imageData) => {
      this.toko[mode] = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      console.log(err);
    });
  }

  ambilPustaka(mode) {
    const options: CameraOptions = {
      quality: 100,
      targetWidth: 500,
      targetHeight: 500,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      encodingType: this.camera.EncodingType.JPEG,
      allowEdit: true
    }
    this.camera.getPicture(options).then((imageData) => {
      this.toko[mode] = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      console.log(err);
    });
  }

}
