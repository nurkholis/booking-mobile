import { Component } from '@angular/core';
import { NavController, NavParams, Events, LoadingController, AlertController, ActionSheetController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { HelperProvider } from '../../providers/helper/helper';
import { Camera, CameraOptions } from '@ionic-native/camera';

@Component({
  selector: 'page-toko-jasa-form',
  templateUrl: 'toko-jasa-form.html',
})
export class TokoJasaFormPage {
  apiUrl:String;
  id_jasa:string;
  title:String;
  subKategoriList = [];
  jasa = {
    id_sub_kategori_jasa: null,
    nama_jasa: null,
    deskripsi_jasa: null,
    harga_jasa: null,
    gambar: null,
    gambar_edit: null,
    _method: 'POST',
  }
  temp_foto = [];
  loading:Boolean;
  sending:Boolean;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public events: Events,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public api: ApiProvider,
    public helper: HelperProvider,
    public actionSheetCtrl: ActionSheetController,
    private camera: Camera,
  ) {
    
  }

  ionViewDidLoad() {
    this.apiUrl = this.api.apiUrl;
    this.id_jasa = this.navParams.get('id_jasa');
    if(!this.id_jasa){
      this.title = 'Tambah';
    }else{
      this.title = 'Edit';
      this.jasa._method = 'PUT';
      this.getJasa(this.id_jasa);
    }
    this.getKategori();
  }

  ionViewWillLeave(){
  }
  
  getKategori(){
    this.loading = true;
    let param = {
      id_kategori_jasa: localStorage.getItem('id_kategori_jasa')
    }
    this.api.get('sub.kategori.jasa', param).then((r)=>{
      this.subKategoriList = r['data'];
      this.loading = false;
    }).catch((e)=>{
      this.helper.msg('Gagal memuat kategori');
      this.loading = false;
    });
  }

  getJasa(id_jasa){
    let param = {
      id_jasa : id_jasa,
    }
    this.api.get('jasa', param).then((r)=>{
      this.jasa.nama_jasa = r['data'][0]['nama_jasa'];
      this.jasa.harga_jasa = r['data'][0]['harga_jasa'];
      this.jasa.deskripsi_jasa = r['data'][0]['deskripsi_jasa'];
      this.jasa.gambar_edit = r['data'][0]['gambar'];
      this.jasa.id_sub_kategori_jasa = r['data'][0]['sub_kategori_jasa']['id_sub_kategori_jasa'];
    }).catch((e)=>{
      this.helper.msg('Gagal memuat jasa');
    });
  }

  ambilPustaka() {
    const options: CameraOptions = {
      quality: 100,
      targetWidth: 500,
      targetHeight: 500,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      encodingType: this.camera.EncodingType.JPEG,
      allowEdit: true
    }
    this.camera.getPicture(options).then((imageData) => {
      this.jasa.gambar = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      console.log(err);
    });
  }

  simpan(){
    let valid = true;
    if(this.title == 'Tambah'){
      valid = this.helper.validate(this.jasa, {
        'nama_jasa': 'required',
        'harga_jasa': 'required',
        'id_sub_kategori_jasa': 'required',
        'deskripsi_jasa': 'required',
        'gambar': 'required',
      });
    }
    if(valid){
      let header = {
        Authorization  : 'Bearer ' + localStorage.getItem('token'),
      }
      this.sending = true;
      this.api.post('jasa', this.jasa, header, this.id_jasa).then((r)=>{
        this.events.publish('app:reload-getJasa');
        this.sending = false;
        this.helper.msg('Berhasil menyimpan jasa');
        this.navCtrl.pop();
      }).catch((e)=>{
        this.sending = false;
        this.helper.alert('Gagal', this.helper.error(e));
      })
    }
  }

  edit(){

  }

}
