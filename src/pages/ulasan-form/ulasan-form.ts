import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, LoadingController, AlertController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { HelperProvider } from '../../providers/helper/helper';

@Component({
  selector: 'page-ulasan-form',
  templateUrl: 'ulasan-form.html',
})
export class UlasanFormPage {
  id_jasa   :any;
  jasaItem  :any;
  apiUrl:String;
  id_ulasan = null;
  sending: boolean;
  ulasan = {
    komentar: null,
    rating: 0,
    id_jasa: null,
    _method: 'POST',
  }

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public MenuCtrl: MenuController,
    public api: ApiProvider,
    public LoadingCtrl: LoadingController,
	  public helper: HelperProvider,
    public alertCtrl: AlertController,
  ) {
  }

  ionViewDidLoad() {
    this.id_jasa = this.navParams.get('id_jasa');
    this.apiUrl = this.api.apiUrl;  
    this.ulasan.id_jasa=  this.id_jasa;
    this.getUlasan();
    this.getJasaId(this.id_jasa);
  }

  setRaiting(a){
    this.ulasan.rating = a;
  }

  getUlasan(){
    let param = {
      id_jasa : this.id_jasa,
      id_member : localStorage.getItem('id_member')
    }
    this.api.get('ulasan', param).then((r)=>{
      // jika ada data, maka method put
      this.ulasan.komentar = r['data'][0]['komentar'];
      this.ulasan.rating = parseInt(r['data'][0]['rating']);
      this.id_ulasan = parseInt(r['data'][0]['id_ulasan']);
      this.ulasan._method = 'PUT';
    }).catch((e)=>{
      // this.helper.msg('Gagal memuat data ulasan');
    });
  }

  getJasaId(id_jasa){
    let loader = this.LoadingCtrl.create({
      content : 'Mohon menunggu...'
    });
    loader.present().then(()=>{
      let param = {id_jasa: id_jasa};
      this.api.get('jasa', param).then((r)=>{
        this.jasaItem= r['data'][0];
        loader.dismiss();

      }).catch((e)=>{
        loader.dismiss();
      });
    });
  }

  simpan(){
    if(this.ulasan.rating == 0){
      this.helper.msg('Raiting bintang tidak boleh kosong');
      return
    }
    let head =  {
      Authorization  : 'Bearer ' + localStorage.getItem('token')
    }
    let id = this.id_ulasan == null ? '' : this.id_ulasan;
    this.sending = true;
    this.api.post('ulasan', this.ulasan, head, id).then((r)=>{
      this.helper.msg('Berhasil menyimpan rating');
      this.navCtrl.pop();
      this.sending = false;
    }).catch((e)=>{
      this.helper.msg('Gagal menyimpan rating');
      this.sending = false;
    });
  }

}
