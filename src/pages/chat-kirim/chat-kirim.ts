import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Refresher, Content } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { HelperProvider } from '../../providers/helper/helper';

@Component({
  selector: 'page-chat-kirim',
  templateUrl: 'chat-kirim.html',
})
export class ChatKirimPage {
  @ViewChild(Refresher) refresher: Refresher;
  @ViewChild(Content) content: Content;
  id_penerima: '2';
  apiUrl: string;
  chat = {
    isi_chat: null,
    id_penerima: null
  }
  id_member = localStorage.getItem('id_member');
  loading:boolean;
  tanggal: string;
  pesanList = [];
  nama: string;
  foto: string;
  timer = setInterval(()=>{
    this.getPesan();
    this.dilihat();
  }, 5000);
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiProvider,
    public helper: HelperProvider,
  ) {
  }

  ionViewDidLoad() {
    this.id_penerima = this.navParams.get('id_penerima');
    this.nama = this.navParams.get('nama');
    this.foto = this.navParams.get('foto');
    this.chat.id_penerima = this.id_penerima;
    this.apiUrl = this.api.apiUrl;
    this.refresher._top = '60px';
    this.refresher._beginRefresh();
    this.tanggal = this.helper.getTanggal();
  }

  ionViewDidLeave() {
    clearInterval(this.timer);
  }

  scroolToBottom() {
    let dimensions = this.content.getContentDimensions();
    this.content.scrollTo(0, dimensions.contentHeight * dimensions.contentHeight);
  }

  doRefresh(event) {
    this.getPesan();
    this.dilihat();
  }

  dilihat(){
    let param = {
      id_pengirim: this.id_penerima,
      id_penerima: localStorage.getItem('id_member')
    }
    this.api.get('chat.dilihat', param).then((r) => {
    }).catch(() => {
    });
  }

  getPesan(){
    let param = {
      id_penerima: this.id_penerima,
      id_pengirim: localStorage.getItem('id_member')
    }
    this.api.get('chat', param).then((r) => {
      this.pesanList = r['data'];
      this.refresher.complete();
      this.scroolToBottom();
    }).catch(() => {
      this.helper.msg('Gagal memuat data')
      this.refresher.complete();
    });
  }

  kirim(){
    let v = this.helper.validate(this.chat, {
      'isi_chat': 'required'
    });
    if(!v) { return}
    let header = {
      Authorization  : 'Bearer ' + localStorage.getItem('token'),
    }
    this.loading = true;
    this.api.post('chat', this.chat, header).then(()=>{
      this.refresher._beginRefresh();
      this.scroolToBottom();
      this.chat.isi_chat = null;
      this.loading = false;
    }).catch(()=>{
      this.loading = false;
    })
  }

}
