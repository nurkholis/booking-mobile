import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Refresher, Events, App } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { HelperProvider } from '../../providers/helper/helper';
import { TokoJasaFormPage } from '../toko-jasa-form/toko-jasa-form';
import { TokoDaftarPage } from '../toko-daftar/toko-daftar';
@Component({
  selector: 'page-toko-jasa',
  templateUrl: 'toko-jasa.html',
})
export class TokoJasaPage {
  @ViewChild(Refresher) refresher: Refresher;
  dataList = [];
  nextLink:String;
  apiUrl = localStorage.getItem('apiUrl');
  limit = 10;
  open = false;
  cari:String;
  loading:Boolean;
  status_pejasa:string;
  loop2 = setInterval(()=>{
    this.looping();
  }, 5000);
  id_pejasa = localStorage.getItem('id_pejasa');
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public api: ApiProvider,
    private helper: HelperProvider,
    public events: Events,
    public app: App,
    ) {
  }

  ionViewDidLoad() {
    if(this.id_pejasa){
      this.refresher._top = '60px';
      this.events.subscribe('app:reload-getJasa', () => {
        this.refresher._beginRefresh();
      });
      this.refresher._beginRefresh();
    }
  }

  getStatus(){
    this.status_pejasa = localStorage.getItem('status_pejasa');
    let param = {
      id_pejasa : this.id_pejasa
    }
    this.api.get('pejasa', param).then((r) => {
      localStorage.setItem('status_pejasa', r['data'][0]['status_pejasa']);
    }).catch(() => {
    })
  }

  looping(){
    this.status_pejasa = localStorage.getItem('status_pejasa');
    if( this.status_pejasa == '1' || !this.id_pejasa){
      clearInterval(this.loop2);
    }else{
      let param = {
        id_pejasa : this.id_pejasa
      }
      this.api.get('pejasa', param).then((r) => {
        localStorage.setItem('status_pejasa', r['data'][0]['status_pejasa']);
      }).catch(() => {
      })
    }
  }

  bukaJasa(){
    if (localStorage.getItem('status') == '0') {
      this.helper.alert('Gagal', 'Verifikasi dulu akunmu !');
      return;
    }
    this.app.getRootNav().setRoot(TokoDaftarPage);
  }

  getData(prm = null){
    let param:any;
    if(prm!=null){
      param = prm;
    }else{
      param = {
        id_pejasa: localStorage.getItem('id_pejasa'),
        limit: this.limit,
        page: 1,
      }
    }
    this.api.get('jasa', param).then((r)=>{
      this.dataList = r['data'];
      this.nextLink = r['meta']['pagination']['links']['next'];   
      this.refresher.complete();
    }).catch(()=>{
      this.helper.msg('Gagal memuat data')
      this.refresher.complete();
    });
  }

  delete(id_jasa){
    this.helper.confirm('Konfirmasi', 'Apakah anda yakin akan menghapus jasa ?', 'HAPUS', ()=>{
      let header = {
        Authorization  : 'Bearer ' + localStorage.getItem('token'),
      }
      this.api.delete('jasa', id_jasa, null, header).then((r)=>{
        this.refresher._beginRefresh();
        this.refresher.complete();
        this.helper.msg('Berhasil menghapus jasa')
      }).catch(()=>{
        this.helper.msg('Gagal menghapus jasa')
      });
    });
  }

  edit(id_jasa){
    this.navCtrl.push(TokoJasaFormPage, {id_jasa:id_jasa});
  }

  tambah(){
    this.navCtrl.push(TokoJasaFormPage);
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    let a = this.nextLink;
    if(!a){
      infiniteScroll.complete();
      return;
    }
    this.api.infinite(a).then((r)=>{
      infiniteScroll.complete();
      for(let i in r['data']){
        this.dataList.push(r['data'][i]);
      }
      console.log(this.dataList);
      if(r['meta']['pagination']['links']['next']){
        this.nextLink = r['meta']['pagination']['links']['next'];
      }else{
        this.nextLink = null;
      }
      infiniteScroll.complete();
    }).catch((e)=>{
    });
  }

  onCancelSearch() {
    this.getData();
  }

  searchByKeyword(){
    this.search();
  }

  search(){
    let param = {
      cari : this.cari,
      token: localStorage.getItem('token'),
    }
    this.getData(param);
  }

  showSearch(){
    this.open = !this.open;
    this.cari = null;
    if(!this.open){
      this.getData();
    }
  }

  doRefresh(event){
   this.getData();
   this.getStatus();
  }

}
