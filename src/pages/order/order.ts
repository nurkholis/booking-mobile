import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, LoadingController, AlertController, Events } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { HelperProvider } from '../../providers/helper/helper';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { DetailTokoPage } from '../detail-toko/detail-toko';
import { ChatKirimPage } from '../chat-kirim/chat-kirim';
import { LoginPage } from '../login/login';
import { UlasanPage } from '../ulasan/ulasan';
@Component({
  selector: 'page-order',
  templateUrl: 'order.html',
})
export class OrderPage {
  keranjangList = [];
  id_jasa: any;
  jasaItem: any;
  apiUrl = localStorage.getItem("apiUrl");
  id_member = localStorage.getItem("id_member");
  isMe = false;
  loading: Boolean;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public MenuCtrl: MenuController,
    public Api: ApiProvider,
    public LoadingCtrl: LoadingController,
    public helper: HelperProvider,
    public alertCtrl: AlertController,
    public events: Events,
    private photoViewer: PhotoViewer,
  ) {
  }

  ionViewDidLoad() {
    this.id_jasa = this.navParams.get('id_jasa');
    this.keranjangList = JSON.parse(localStorage.getItem('keranjang'));
    this.getJasaId(this.id_jasa);
  }

  lihatToko(id_pejasa){
    this.navCtrl.push(DetailTokoPage, {id_pejasa: id_pejasa});
  }

  getJasaId(id_jasa) {
    let param = { id_jasa: id_jasa };
    this.loading = true;
    this.Api.get('jasa', param).then((r) => {
      this.jasaItem = r['data'][0];
      console.log(this.jasaItem);
      if (localStorage.getItem('id_pejasa') == r['data'][0]['pejasa']['id_pejasa']) {
        this.isMe = true;
      }
      this.loading = false;
    }).catch((e) => {
      this.loading = false;
    });
  }

  ulasan(id_jasa, nama_jasa)
  {
    this.navCtrl.push(UlasanPage, {id_jasa: id_jasa, nama_jasa:nama_jasa});
  }

  tanya(id_penerima, nama, foto) {
    if(this.id_member){
      this.navCtrl.push(ChatKirimPage, {id_penerima: id_penerima, nama: nama, foto:  foto});
    }else{
      this.navCtrl.push(LoginPage);
    }
  }

  view(foto, keterangan = '') {
    this.photoViewer.show(this.apiUrl + '/public/images/jasa/gambar/' + foto, keterangan, { share: true });
  }

  back() {
    this.navCtrl.pop();
  }

  cekJasaDiKeranjang(id_jasa) {
    for (let i in this.keranjangList) {
      if (this.keranjangList[i].id_jasa == id_jasa) {
        return true;
      }
    }
    return false;
  }

  addToCart() {
    //cek jasa sudah ada dalama keranjang
    if (this.cekJasaDiKeranjang(this.id_jasa)) {
      this.helper.alert('Gagal', 'Jasa sudah ada dalam keranjang !');
      return;
    }
    if (localStorage.getItem('status') == '0') {
      this.helper.alert('Gagal', 'Verifikasi dulu akunmu !');
      return;
    }
    let a = {
      id_jasa: this.jasaItem.id_jasa,
      gambar: this.jasaItem.gambar,
      nama_jasa: this.jasaItem.nama_jasa,
    }
    this.keranjangList.push(a);
    localStorage.setItem('keranjang', JSON.stringify(this.keranjangList));
    this.events.publish('home:reload-getJumlahKeranjang');
    return this.helper.alert('Berhasil!', 'Berhasil menambah jasa dalam pesanan');
  }

}