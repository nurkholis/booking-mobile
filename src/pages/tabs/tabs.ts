import { Component } from '@angular/core';
import { ProfilPage } from '../profil/profil';
import { HomePage } from '../home/home';
import { LoginPage } from '../login/login';
import { KeranjangPage } from '../keranjang/keranjang';
import { TransaksiPage } from '../transaksi/transaksi';
import { TokoJasaPage } from '../toko-jasa/toko-jasa';
import { NavParams } from 'ionic-angular';
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  profilPage: any;
  homePage: any;
  keranjangPage: any;
  transaksiPage: any;
  jasaPage: any;
  index = 0;
  constructor(
    private navParams: NavParams,
  ) {
    this.init();
  }

  init() {
    // if (this.navParams.get('index')) {
    //   this.index = this.navParams.get('index');
    // }
    this.homePage = HomePage;
    this.keranjangPage = KeranjangPage;
    if (localStorage.getItem('id_member')) {
      this.profilPage = ProfilPage;
      this.jasaPage = TokoJasaPage;
      this.transaksiPage = TransaksiPage;
    } else {
      this.profilPage = LoginPage;
      this.jasaPage = LoginPage;
      this.transaksiPage = LoginPage;
    }
  }
}