import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Events, Refresher, App } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { HelperProvider } from '../../providers/helper/helper';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-profil',
  templateUrl: 'profil.html',
})
export class ProfilPage {
  @ViewChild(Refresher) refresher: Refresher;
  apiUrl: string;
  loading: boolean;
  id_pejasa = localStorage.getItem('id_pejasa');
  akun = {
    nama: null,
    email: null,
    foto: null,
    img: null,
    gender: null,
    telepon: null,
    _METHOD: 'put',
  }
  password = {
    email: null,
    password_lama : null,
    password_baru: null,
    password_konfirmasi: null
  }
  toko = {
    nama_toko: null,
    img: null,
    logo: null,
  }
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiProvider,
    public helper: HelperProvider,
    public events: Events,
    private camera: Camera,
    private app: App,
  ) {
  }

  ionViewDidLoad() {
    this.apiUrl = this.api.apiUrl;
    this.refresher._top = '60px';
    this.refresher._beginRefresh();
  }

  doRefresh(event) {
    this.getAkun();
  }

  logout() {
    this.helper.confirm('Konfirmasi', 'Apakah anda yakin ingin logout ?', 'Logout', () => {
      let url = localStorage.getItem('apiUrl');
      localStorage.clear();
      localStorage.setItem('apiUrl', url);
      localStorage.setItem('keranjang', '[]');
      this.app.getRootNav().setRoot(TabsPage);
    });
  }

  getAkun() {
    this.loading = true;
    let header = {
      Authorization: 'Bearer ' + localStorage.getItem('token'),
    }
    this.api.get('member.profil', null, header).then((r) => {
      this.akun.nama = r['data']['nama'];
      this.akun.email = r['data']['email'];
      this.akun.gender = r['data']['gender'];
      this.akun.telepon = r['data']['telepon'];
      this.akun.img = r['data']['foto'];
      this.password.email = r['data']['email'];
      this.refresher.complete();
      this.loading = false;
    }).catch((e) => {
      this.refresher.complete();
      this.loading = false;
    })
    if (this.id_pejasa) {
      let param = {
        id_pejasa: this.id_pejasa
      }
      this.api.get('pejasa', param).then((r) => {
        this.toko.img = r['data'][0]['logo'];
        this.toko.nama_toko = r['data'][0]['nama_toko'];
      }).catch((e) => {
      })
    }
  }

  simpan() {
    this.helper.confirm('Konfirmasi', 'Apakah anda yakin menyimpan profil ?', 'Simpan', () => {
      let l = this.helper.loader();
      l.present().then(() => {
        let header = {
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        }
        this.api.post('member.edit', this.akun, header).then(() => {
          l.dismiss();
          this.helper.msg('Berhasil menyimpan profil');
        }).catch((err) => {
          l.dismiss();
          this.helper.msg('Gagal menyimpan profil');
        });
      });
    })
  }

  simpanToko() {
    this.helper.confirm('Konfirmasi', 'Apakah anda yakin menyimpan profil ?', 'Simpan', () => {
      let l = this.helper.loader();
      l.present().then(() => {
        let header = {
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        }
        this.api.post('member.edit', this.akun, header).then(() => {
          l.dismiss();
          this.helper.msg('Berhasil menyimpan profil');
        }).catch((err) => {
          l.dismiss();
          this.helper.msg('Gagal menyimpan profil');
        });
      });
    })
  }

  ambilPustaka(mode) {
    const options: CameraOptions = {
      quality: 100,
      targetWidth: 500,
      targetHeight: 500,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      encodingType: this.camera.EncodingType.JPEG,
      allowEdit: true
    }
    this.camera.getPicture(options).then((ImageData) => {
      if (mode == 'foto') {
        this.akun.foto = 'data:image/jpeg;base64,' + ImageData;
        this.akun.img = null;
      } else {
        this.toko.logo = 'data:image/jpeg;base64,' + ImageData;
        this.toko.img = null;
      }
    }, (err) => {
      console.log(err);
    });
  }

  resetPassword() {
    let l = this.helper.loader();
    l.present().then(() => {
      if (this.password.password_lama != this.password.password_baru) {
        this.helper.msg('Password konfirmasi tidak sama');
        l.dismiss();
        return;
      }
      this.api.post('member.pw.reset', this.password).then((r) => {
        localStorage.setItem('token', r['meta']['token']);
        this.helper.msg('Berhasil mengganti password');
        l.dismiss();
      }).catch((e) => {
        this.helper.msg('Gagal mengganti password');
        l.dismiss();
      })
    });
  }

}
