import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, LoadingController, Events, App } from 'ionic-angular';

import { ApiProvider } from '../../providers/api/api';
import { HttpClient } from '@angular/common/http';
import { HelperProvider } from '../../providers/helper/helper';
import { DaftarPage } from '../daftar/daftar';
import { TabsPage } from '../tabs/tabs';


@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  email:any;
  password:any;
  akun = {
    email : '',
    password : ''
  };
  mode:string;
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public http: HttpClient, 
    public api: ApiProvider,
    public menu: MenuController,
    private helper: HelperProvider,
    private loadingCtrl: LoadingController,
    private events: Events,
    private app: App,
  ) {
  }

  ionViewDidLoad() {
    // this.menu.swipeEnable(false);
  }

  back(){
    this.app.getRootNav().setRoot(TabsPage);
  }

  login(){
    let loader = this.loadingCtrl.create({
      content: "Mohon menunggu..."
    });
    loader.present().then(()=>{
      this.api.post('member.login', this.akun).then((r)=>{
        loader.dismiss();
        localStorage.setItem('id_member', r['data']['id_member']);
        localStorage.setItem('token', r['meta']['token']);
        localStorage.setItem('email', r['data']['email']);
        localStorage.setItem('status', r['data']['status']);
        localStorage.setItem('id_kategori_jasa', r['data']['id_kategori_jasa']);
        if(r['data']['pejasa']){
          localStorage.setItem('id_pejasa', r['data']['pejasa']['id_pejasa']);
          localStorage.setItem('nama_toko', r['data']['pejasa']['nama_toko']);
          localStorage.setItem('status_pejasa', r['data']['pejasa']['status_pejasa']);
          localStorage.setItem('id_kategori_jasa', r['data']['kategori_jasa']['id_kategori_jasa']);
        }
        this.app.getRootNav().setRoot(TabsPage);
      }).catch((e)=>{
        loader.dismiss();
        this.helper.alert('Gagal !', this.helper.error(e))
      });
    });
  }

  daftar(){
    this.navCtrl.push(DaftarPage);
  }

}
