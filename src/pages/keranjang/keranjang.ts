import { Component } from '@angular/core';
import { NavController, NavParams, MenuController, LoadingController, AlertController, Events } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { HelperProvider } from '../../providers/helper/helper';
import { LoginPage } from '../login/login';

@Component({
  selector: 'page-keranjang',
  templateUrl: 'keranjang.html',
})
export class KeranjangPage {
  keranjangList = [];
  apiUrl: String;
  id_member = localStorage.getItem('id_member');
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public api: ApiProvider,
    public helper: HelperProvider,
    public menuCtrl: MenuController,
    public loaderCtrl: LoadingController,
    public alertCtrl: AlertController,
    public events: Events,
  ) {
    this.events.subscribe('app:reload-getKeranjang', () => {
      this.getKeranjang();
    });
  }

  ionViewDidLoad() {
    this.getKeranjang();
    this.apiUrl = localStorage.getItem('apiUrl');
  }

  getKeranjang() {
    this.keranjangList = JSON.parse(localStorage.getItem('keranjang'));
    console.log(this.keranjangList);
  }

  post(i) {
    if (!this.id_member) {
      this.helper.confirm('Peringatan', 'Anda harus login untuk melanjutkan', 'login', () => {
        this.navCtrl.push(LoginPage);
      });
    } else {
      let id_jasa = this.keranjangList[i]['id_jasa'];
      let nama_jasa = this.keranjangList[i]['nama_jasa'];
      let header = {
        Authorization: 'Bearer ' + localStorage.getItem('token'),
      }
      let param = {
        id_jasa: id_jasa,
      }
      let loader = this.helper.loader();
      // post transaksi
      this.helper.confirm('Konfirmasi', 'Lanjutkan untuk memesan ' + nama_jasa + ' ?', 'Lanjukan', () => {
        loader.present().then(() => {
          this.api.post('transaksi', param, header).then(() => {
            loader.dismiss();
            this.hapus(i);
            this.helper.msg('Berhasil booking jasa');
            this.navCtrl.pop();
          }).catch(() => {
            loader.dismiss();
            this.helper.msg('Gagal booking jasa');
          })
        });
      });
    }
  }

  KonfirmasiHapus(i) {
    let nama_jasa = this.keranjangList[i]['nama_jasa'];
    this.helper.confirm('Konfirmasi', 'Lanjutkan untuk menghapus ' + nama_jasa + ' ?', 'Lanjukan', () => {
      this.hapus(i);
    });
  }

  hapus(i) {
    this.keranjangList.splice(i, 1);
    localStorage.setItem('keranjang', JSON.stringify(this.keranjangList));
    this.events.publish('home:reload-getJumlahKeranjang');
  }

}
