import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';

@Component({
  selector: 'page-developer',
  templateUrl: 'developer.html',
})
export class DeveloperPage {
  setting = {
    apiUrl: localStorage.getItem('apiUrl'),
    notifikasiInternal: localStorage.getItem('notifikasiInternal'),
  }
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    private helper: HelperProvider,
    ) {
  }

  ionViewDidLoad() {
  }

  simpan(){
    localStorage.setItem('apiUrl', this.setting.apiUrl);
    localStorage.setItem('notifikasiInternal', this.setting.notifikasiInternal);
    this.helper.alert('Berhasil', 'berhasil menyimpan pengaturan!')
  }

}
