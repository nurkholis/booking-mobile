import { Component } from '@angular/core';
import { NavController, LoadingController, MenuController, AlertController, Searchbar } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { HelperProvider } from '../../providers/helper/helper';
import { KeranjangPage } from '../keranjang/keranjang';
import { ViewChild } from '@angular/core';
import { OrderPage } from '../order/order';

@Component({
  selector: 'page-cari',
  templateUrl: 'cari.html'
})
export class CariPage {
  @ViewChild('searchbar') searchbar: Searchbar;
  JasaList = [];
  nextLink: String;
  apiUrl: any;
  id_kategori_jasa = null;
  cari: String;
  empty_data = false;
  constructor(
    public navCtrl: NavController,
    public LoadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public Api: ApiProvider,
    public helper: HelperProvider,
    public MenuCtrl: MenuController,
  ) {
    this.apiUrl = this.Api.apiUrl;
  }

  ionViewDidLoad() {
    setTimeout(() => {
      this.searchbar.setFocus();
    }, 600);
    this.MenuCtrl.swipeEnable(true);
    // this.getBarang();
  }

  order(id_jasa) {
    this.navCtrl.push(OrderPage, { id_jasa: id_jasa });
  }

  getBarang(param = null) {
    this.id_kategori_jasa = null;
    this.nextLink = null
    let loader = this.LoadingCtrl.create({
      content: 'Mohon Menunggu...'
    });
    loader.present().then(() => {
      this.Api.get('jasa', param).then((r) => {
        this.JasaList = r['data'];
        this.nextLink = r['meta']['pagination']['links']['next'];
        if (this.JasaList.length == 0) {
          this.empty_data = true;
        } else {
          this.empty_data = false;
        }
        loader.dismiss();
      }).catch((e) => {
        loader.dismiss();
      });
    });
  }

  ribuan(nStr) {
    return this.helper.ribuan(nStr);
  }

  keranjang() {
    this.navCtrl.push(KeranjangPage);
  }

  doInfinite(infiniteScroll) {
    console.log('Begin async operation');
    let a = this.nextLink;
    if (!a) {
      infiniteScroll.complete();
      return;
    }
    this.Api.infinite(a).then((r) => {

      infiniteScroll.complete();
      for (let i in r['data']) {
        this.JasaList.push(r['data'][i]);
      }
      console.log(this.JasaList);
      if (r['meta']['pagination']['links']['next']) {
        this.nextLink = r['meta']['pagination']['links']['next'];
      } else {
        this.nextLink = null;
      }
      infiniteScroll.complete();
    }).catch((e) => {
    });
  }

  searchByKeyword() {
    let param = {
      cari: this.cari,
      status: '1',
    }
    this.getBarang(param);
  }

}
