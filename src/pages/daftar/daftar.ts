import { Component } from '@angular/core';
import { NavController, NavParams, Platform, ActionSheetController, App } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { ApiProvider } from '../../providers/api/api';
import { DatePicker } from '@ionic-native/date-picker';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-daftar',
  templateUrl: 'daftar.html',
})
export class DaftarPage {
  member ={
    nama: null,
    email: null,
    gender: 'L',
    alamat: null,
    tanggal_lahir: '2019-09-09',
    tempat_lahir: null,
    telepon: null,
    foto: null,
    password: null,
    password_konfirmasi: null,
    accept: null,
  }
  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public helper: HelperProvider,
    public api: ApiProvider,
    public platform: Platform,
    public datePicker: DatePicker,
    public actionSheetCtrl: ActionSheetController,
    private camera: Camera,
    public app: App,
    ) {
  }

  ionViewDidLoad() {
    this.platform.ready().then(()=>{
      console.log('plat form ready');
    })
  }

  back(){
    this.navCtrl.pop();
  }

  daftar(){
    if(this.member.password != this.member.password_konfirmasi){
      this.helper.msg('Konfirmasi password tidak sama');
      return;
    }
    let l = this.helper.loader();
    l.present().then(()=>{
      this.api.post('member', this.member).then((r)=>{
        l.dismiss();
        localStorage.setItem('id_member', r['data']['id_member']);
        localStorage.setItem('token', r['meta']['token']);
        localStorage.setItem('email', r['data']['email']);
        localStorage.setItem('status', r['data']['status']);
        localStorage.setItem('id_kategori_jasa', r['data']['id_kategori_jasa']);
        this.helper.confirm2('Berhasil', 'Pendaftaran berhasil, kami mengirimkan link verifikasi ke emailmu. Konfrimasi sekarang untuk melanjutkan.', 'Ok', ()=>{
          this.app.getRootNav().setRoot(TabsPage);
        })
      }).catch((e)=>{
        l.dismiss();
        this.helper.alert('Gagal', this.helper.error(e));
      })
    });
  }

  ambilTanggal(){
    this.datePicker.show({
      date: new Date(),
      mode: 'date',
      androidTheme: this.datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT 
    }).then((date)=>{
      let today:any = date;
      let dd:any = today.getDate();
      let mm:any = today.getMonth()+1; //January is 0!
      let yyyy:any = today.getFullYear();
      if(dd<10){
          dd='0'+dd;
      } 
      if(mm<10){
          mm='0'+mm;
      } 
      let jadi = yyyy+'-'+mm+'-'+dd;
      this.member.tanggal_lahir = jadi;
    });
  }

  ambilGambar(){
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Pilih Sumber Gambar',
      buttons: [
        {
          text: 'Dari Pustaka',
          icon: 'image',
          handler: () => {
            this.ambilPustaka();
          }
        },
        {
          text: 'Dari Kamera',
          icon: 'camera',
          handler: () => {
            this.ambilFoto();
          }
        }, {
          text: 'Batal',
          role: 'cancel'
        }
      ]
    });
      actionSheet.present();
  }

  ambilFoto() {
    const options: CameraOptions = {
      quality: 100,
      targetWidth: 500,
      targetHeight: 500,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      saveToPhotoAlbum: true,
      allowEdit: true,
    }
    this.camera.getPicture(options).then((imageData) => {
      this.member.foto = 'data:image/jpeg;base64,' + imageData;
    }, (err) => {
      console.log(err);
    });
  }

  ambilPustaka() {
    const options: CameraOptions = {
      quality: 100,
      targetWidth: 500,
      targetHeight: 500,
      destinationType: this.camera.DestinationType.DATA_URL,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      encodingType: this.camera.EncodingType.JPEG,
      allowEdit: true
    }
    this.camera.getPicture(options).then((ImageData) => {
      this.member.foto = 'data:image/jpeg;base64,' + ImageData;
    }, (err) => {
      console.log(err);
    });
  }

}
