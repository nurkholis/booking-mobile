import { Component } from '@angular/core';
import { NavController, NavParams, LoadingController } from 'ionic-angular';
import { ApiProvider } from '../../providers/api/api';
import { HelperProvider } from '../../providers/helper/helper';

@Component({
  selector: 'page-ulasan',
  templateUrl: 'ulasan.html',
})
export class UlasanPage {


  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public api: ApiProvider,
    public helper: HelperProvider,
    ) {
  }
  ulasanList:any;
  apiUrl:String;
  nextLink:String;
  id_jasa = null;
  nama_jasa = null;
  arrRaiting = [1,2,3,4,5];
  ionViewDidLoad() {
    console.log('ionViewDidLoad UlasanPage');
    // this.id_jasa = '7';
    this.id_jasa = this.navParams.get('id_jasa');
    this.nama_jasa = this.navParams.get('nama_jasa');
    this.getUlasan(this.id_jasa);

  }

  doRefresh(event){
    let param = {
      limit : 7,
      id_jasa : this.id_jasa,
    }
    this.api.get('ulasan', param).then((r)=>{
      this.ulasanList = r['data'];
      this.nextLink = r['meta']['pagination']['links']['next'];   
      event.complete();
    }).catch((e)=>{
      this.helper.msg('Gagal memuat jasa');
      event.complete();
    });
  }

  getUlasan(id_jasa){
    let loader = this.loadingCtrl.create({
      content: 'Mohon tunggu...'
    })
    loader.present().then(()=>{
      let param = {
        limit : 7,
        id_jasa : id_jasa,
      }
      this.api.get('ulasan', param).then((r)=>{
        this.ulasanList = r['data'];
        this.nextLink = r['meta']['pagination']['links']['next'];   
        loader.dismiss();
      }).catch((e)=>{
        loader.dismiss();
      });
    });
  }

}
