import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Refresher } from 'ionic-angular';
import { HelperProvider } from '../../providers/helper/helper';
import { ApiProvider } from '../../providers/api/api';
import { ChatKirimPage } from '../chat-kirim/chat-kirim';
import { LoginPage } from '../login/login';
import { OrderPage } from '../order/order';

@Component({
  selector: 'page-detail-toko',
  templateUrl: 'detail-toko.html',
})
export class DetailTokoPage {
  @ViewChild(Refresher) refresher: Refresher;
  id_pejasa:any;
  pejasa = [];
  jasa = [];
  apiUrl: any;
  id_member = localStorage.getItem('id_member');
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public helper: HelperProvider,
    public api: ApiProvider,
  ) {
  }

  ionViewDidLoad() {
    this.apiUrl = this.api.apiUrl;
    this.refresher._beginRefresh();
  }

  doRefresh(){
    this.id_pejasa = this.navParams.get('id_pejasa');
    this.getToko();
    this.getJasa();
  }

  order(id_jasa) {
    this.navCtrl.push(OrderPage, { id_jasa: id_jasa });
  }


  getToko(){
    let param = {
      id_pejasa: this.id_pejasa
    };
    this.api.get('pejasa', param).then((r) => {
      this.pejasa = r['data'][0];
      this.refresher.complete();
    }).catch((e) => {
      this.refresher.complete();
      this.helper.msg('gagal membuat data pejasa');
    });
  }

  getJasa(){
    let param = {
      id_pejasa: this.id_pejasa,
      limit: 100,
    };
    this.api.get('jasa', param).then((r)=>{
      this.refresher.complete();
      this.jasa = r['data'];
    }).catch(()=>{
      this.refresher.complete();
      this.helper.msg('Gagal memuat jasa')
    });
  }

  chat(telepon){
    window.location.href = 'https://api.whatsapp.com/send?phone=' + telepon + '&text= ';
  }

  call(telepon){
    // this.callNumber.callNumber(telepon, true);
  }

  tanya(id_penerima, nama, foto) {
    if(this.id_member){
      this.navCtrl.push(ChatKirimPage, {id_penerima: id_penerima, nama: nama, foto:  foto});
    }else{
      this.navCtrl.push(LoginPage);
    }
  }


}
