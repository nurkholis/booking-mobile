import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AlertController, ToastController, LoadingController } from 'ionic-angular';

@Injectable()
export class HelperProvider {

  constructor(
    public http: HttpClient,
    public alertCtrl: AlertController,
    public toastCtrl: ToastController,
    public loadingCtrl: LoadingController,
  ) {
    console.log('Hello HelperProvider Provider');
  }

  public loader() {
    return this.loadingCtrl.create({
      content: 'Mohon menunggu...'
    });
  }

  public jam(jam){
    jam = jam.split(':');
    return jam[0] + ':' + jam[1];
  }

  public getTanggal() {
    let today = new Date();
    let dd = today.getDate();
    let mm = today.getMonth() + 1; //January is 0!
    let yyyy = today.getFullYear();
    let tmm = mm.toString();
    let tdd = dd.toString();
    if (dd < 10) {
      tdd = '0' + dd;
    }
    if (mm < 10) {
      tmm = '0' + mm;
    }
    return yyyy + '-' + tmm + '-' + tdd;
  }

  public alert(subtitle, msg) {
    this.alertCtrl.create({
      message: msg,
      subTitle: subtitle,
      buttons: [{
        text: 'ok'
      }]
    }).present();
  }

  public confirm(title, message, text, handler) {
    let alert = this.alertCtrl.create({
      subTitle: title,
      message: message,
      buttons: [
        {
          text: 'Batal',
          role: 'Cancel'
        },
        {
          text: text,
          handler: handler
        }
      ]
    });
    alert.present();
  }

  public confirm2(title, message, text, handler) {
    let alert = this.alertCtrl.create({
      subTitle: title,
      message: message,
      buttons: [
        {
          text: text,
          handler: handler
        }
      ]
    });
    alert.present();
  }

  public error(err) {
    // return JSON.stringify(err);
    let msg = '';
    let errors = err.error.errors;
    for (let key in errors) {
      let error  = errors[key]
      for(let a in error){
        msg += error[a] + '<br>';
      }
    }
    return msg;
  }

  ribuan(nStr) {
    nStr += '';
    let x = nStr.split('.');
    let x1 = x[0];
    let x2 = x.length > 1 ? '.' + x[1] : '';
    let rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
      x1 = x1.replace(rgx, '$1' + '.' + '$2');
    }
    return x1 + x2;
  }

  msg(msg) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 4000,
      position: 'bottom'
    });
    toast.present();
  }

  bulanTanggal(tanggal) {
    if (!tanggal) {
      return;
    }
    let days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
    let d = new Date(tanggal);
    let dayName = days[d.getDay()];
    //console.log(dayName);
    let a = tanggal.split("-");
    const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni",
      "Juli", "Augustus", "September", "Oktober", "November", "Desember"
    ];
    return dayName + " , " + a[2] + " " + monthNames[a[1] - 1] + " " + a[0];
  }
  bulanTanggal2(tanggal) {
    if (!tanggal) {
      return;
    }
    let x = tanggal.split('/');
    let z = x[2] + "-" + x[1] + "-" + x[0];
    tanggal = z;
    let days = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jumat', 'Sabtu'];
    let d = new Date(tanggal);
    let dayName = days[d.getDay()];
    //console.log(dayName);
    let a = tanggal.split("-");
    const monthNames = ["Januari", "Februari", "Maret", "April", "Mei", "Juni",
      "Juli", "Augustus", "September", "Oktober", "November", "Desember"
    ];
    return dayName + " , " + a[2] + " " + monthNames[a[1] - 1] + " " + a[0];
  }
  hurufPertama(string) {
    if (!string) {
      return;
    }
    return string.split("")[0].toUpperCase();
  }
  tanggal(tanggal, mode) {
    if (!tanggal) {
      return;
    }
    tanggal = tanggal.split(" ");
    if (tanggal.length < 0) {
      return;
    }
    if (mode == 'tanggal') {
      return tanggal[0].split('/')[0]
    } else {
      let a = Number(tanggal[0].split('/')[1])
      const monthNames = ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun",
        "Jul", "Aug", "Sep", "Okt", "Nov", "Des"
      ];
      return monthNames[a - 1];
    }
  }

  validate(request, rules, toastCtrl = this.toastCtrl) {
    let a = true;
    Object.keys(rules).forEach(function (key) {
      let rule = rules[key].split('|');
      rule.forEach(element => {
        if (element == 'required') {
          if (request[key] == null || request[key] == '') {
            let msg = 'isian ' + key + ' harus diisi';
            msg = msg.replace("_", " ");
            toastCtrl.create({ message: msg, position: 'bottom', duration: 3000 }).present();
            a = false;
          }
        }
      });
    });
    return a;
  }
}
