import { Injectable } from '@angular/core';
import { HttpParams, HttpClient, HttpHeaders } from '@angular/common/http';
@Injectable()
export class ApiProvider {
  versi = 1.0;
  endPoint = {
    'member' : '/api/member',
    'member.profil' : '/api/member/profil',
    'member.edit' : '/api/member/edit',
    'member.pw.reset' : '/api/member/pw/reset',
    'chat' : '/api/chat',
    'chat.room' : '/api/chat/room',
    'chat.dilihat' : '/api/chat/dilihat',
    'member.login' : '/api/member/login',
    'pejasa' : '/api/pejasa',
    'jasa' : '/api/jasa',
    'kecamatan' : '/api/kecamatan',
    'kabupaten' : '/api/kabupaten',
    'transaksi' : '/api/transaksi',
    'ulasan' : '/api/ulasan',
    'kategori.jasa' : '/api/kategori/jasa',
    'sub.kategori.jasa' : '/api/sub/kategori/jasa',
  }; 
  public apiUrl = 'https://booking.kodejariah.com';
  // public apiUrl = 'http://localhost/booking';
  constructor(public http: HttpClient) {
    localStorage.setItem('apiUrl', this.apiUrl);
  }

  public get(type:any, param:Object = null, header:Object = null){
    
    return new Promise( (resolve, reject) =>{

      let headers = this.header(header);
      let params = this.param(param);

      this.http.get(this.apiUrl+this.endPoint[type], {params: params , headers: headers}).subscribe(data => {
        resolve(data);
      }, error => {
        reject(error);
      });
    });
  }

  public infinite(url:any, param:Object = null, header:Object = null){
    return new Promise( (resolve, reject) =>{

      let headers = this.header(header);
      let params = this.param(param);

      this.http.get(url, {params: params , headers: headers}).subscribe(data => {
        resolve(data);
      }, error => {
        reject(error);
      });
    });
  }

  public post(type:any, param:Object = null , header:Object = null, id=''){
    id == '' ? id='' : id='/'+id; 
    let headers = this.header(header);
    let params = this.param(param);
    
    return new Promise( (resolve,  reject)=>{
      this.http.post(this.apiUrl+this.endPoint[type]+id, params, {headers: headers}).subscribe(data =>{
        resolve(data);        
      },error=>{
        console.log(error);
        reject(error);
      });
    });

  }

  public delete(type:any, id, param:Object = null, header:Object = null){
    return new Promise( (resolve,  reject)=>{
      let headers = this.header(header);
      let params = this.param(param);
      this.http.delete(this.apiUrl+this.endPoint[type]+"/"+id, {params: params , headers: headers}).subscribe(data =>{
        resolve(data);        
      },error=>{
        console.log(error);
        reject(error);
      });
    });
  }

  public header(header:Object){
    let headers: HttpHeaders = new HttpHeaders();
    if (header != null){
      Object.keys(header).forEach(function(key) {
        if(header[key] != null){
          headers = headers.set(key, header[key]);
        }
      });
      return headers;
    }
    return {};
  }

  public param(param:object){
    let params: HttpParams = new HttpParams();
    if (param != null){
      Object.keys(param).forEach(function(key) {
        if(param[key] != null){
          params = params.append(key, param[key]);
        }
      });
      return params;
    }
    return {};
  }

}
