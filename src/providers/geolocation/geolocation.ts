import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { HelperProvider } from '../helper/helper';
import { Platform } from 'ionic-angular';
@Injectable()
export class GeolocationProvider {

  constructor(
    public locationAccuracy: LocationAccuracy,
    public geolocation: Geolocation,
    private helper: HelperProvider,
    public platform: Platform,
  ) {
  }

  activatedGPS() {
    // if (!this.platform.is('cordova')) {
    //   this.locationAccuracy.canRequest().then((canRequest: boolean) => {
    //     if (canRequest) {
    //       this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(() => {
    //         this.helper.msg('actived');
    //       }, error => {
    //         this.helper.msg('disactived');
    //       });
    //     } else {
    //       this.helper.msg('not can request');
    //     }
    //   });
    // }
  }

  loadLocation() {
    let options = {
      enableHighAccuracy: true,
      timeout: 5000,
      maximumAge: 0
    };
    const watch = this.geolocation.watchPosition(options);
    watch.subscribe((data) => {
      if (data.coords !== undefined) {
        localStorage.setItem('lat', data.coords.latitude.toString());
        localStorage.setItem('long', data.coords.longitude.toString());
      } else {
        // this.helper.msg('gps off');
      }
    });
  }

}
