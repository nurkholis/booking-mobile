import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { InAppBrowser } from '@ionic-native/in-app-browser';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ApiProvider } from '../providers/api/api';
import { HelperProvider } from '../providers/helper/helper';
import { HttpClientModule } from '@angular/common/http';
import { TentangPage } from '../pages/tentang/tentang';
import { LoginPage } from '../pages/login/login';
import { StreamingMedia } from '@ionic-native/streaming-media';
import { NativeAudio } from '@ionic-native/native-audio';
import { DeveloperPage } from '../pages/developer/developer';
import { ProfilPage } from '../pages/profil/profil';
import { DaftarPage } from '../pages/daftar/daftar';
import { DatePicker } from '@ionic-native/date-picker';
import { Camera } from '@ionic-native/camera';
import { TabsPage } from '../pages/tabs/tabs';
import { TokoDaftarPage } from '../pages/toko-daftar/toko-daftar';
import { TokoJasaPage } from '../pages/toko-jasa/toko-jasa';
import { TokoJasaFormPage } from '../pages/toko-jasa-form/toko-jasa-form';
import { OrderPage } from '../pages/order/order';
import { KeranjangPage } from '../pages/keranjang/keranjang';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { TransaksiPage } from '../pages/transaksi/transaksi';
import { DetailPembelianPage } from '../pages/detail-pembelian/detail-pembelian';
import { DetailPenjualanPage } from '../pages/detail-penjualan/detail-penjualan';
import { DetailTokoPage } from '../pages/detail-toko/detail-toko';
import { ChatPage } from '../pages/chat/chat';
import { ChatKirimPage } from '../pages/chat-kirim/chat-kirim';
import { CariPage } from '../pages/cari/cari';
import { FilterPage } from '../pages/filter/filter';
import { GeolocationProvider } from '../providers/geolocation/geolocation';
import { Geolocation } from '@ionic-native/geolocation';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { NumberFormatDirective } from '../directives/number-format/number-format';
import { UlasanPage } from '../pages/ulasan/ulasan';
import { UlasanFormPage } from '../pages/ulasan-form/ulasan-form';

@NgModule({
  declarations: [
    NumberFormatDirective,
    MyApp,
    HomePage,
    TentangPage,
    LoginPage,
    DaftarPage,
    DeveloperPage,
    ProfilPage,
    TabsPage,
    TokoDaftarPage,
    TokoJasaPage,
    TokoJasaFormPage,
    OrderPage,
    KeranjangPage,
    TransaksiPage,
    DetailPembelianPage,
    DetailPenjualanPage,
    DetailTokoPage,
    ChatPage,
    ChatKirimPage,
    CariPage,
    FilterPage,
    UlasanPage,
    UlasanFormPage,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TentangPage,
    LoginPage,
    DaftarPage,
    DeveloperPage,
    ProfilPage,
    TabsPage,
    TokoDaftarPage,
    TokoJasaPage,
    TokoJasaFormPage,
    OrderPage,
    KeranjangPage,
    TransaksiPage,
    DetailPembelianPage,
    DetailPenjualanPage,
    DetailTokoPage,
    ChatPage,
    ChatKirimPage,
    CariPage,
    FilterPage,
    UlasanPage,
    UlasanFormPage,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ApiProvider,
    HelperProvider,
    GeolocationProvider,
    InAppBrowser,
    StreamingMedia,
    NativeAudio,
    DatePicker,
    Camera,
    PhotoViewer,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Geolocation,
    LocationAccuracy,
  ]
})
export class AppModule {}
