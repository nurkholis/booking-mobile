import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, MenuController, Events } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { DeveloperPage } from '../pages/developer/developer';
import { ProfilPage } from '../pages/profil/profil';
import { HelperProvider } from '../providers/helper/helper';
import { TabsPage } from '../pages/tabs/tabs';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) navCtrl: Nav;
  rootPage: any;
  nama:string;

  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen,
    public events: Events,
    public menu: MenuController,
    public helper: HelperProvider,
  ) {
    this.rootPage = TabsPage;
    if(!localStorage.getItem('keranjang')){
      localStorage.setItem('keranjang', '[]');
    }
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
    });
    this.setNama();
    this.events.subscribe('app:reload-nama', ()=>{
      this.setNama();
    });
  }

  setNama(){
    this.nama = localStorage.getItem('nama_admin');
  }

  openDevPage(){
    this.navCtrl.push(DeveloperPage)
    this.menu.toggle();
  }

  profil(){
    this.navCtrl.push(ProfilPage)
    this.menu.toggle();
  }

  logout(){
    this.helper.confirm('Konfirmasi', 'Apakah anda yakin ingin logout ?', 'Logout', ()=>{
      localStorage.clear();
      localStorage.setItem('keranjang', '[]');
      this.menu.toggle();
      this.navCtrl.setRoot(LoginPage);
    });
  }
}

