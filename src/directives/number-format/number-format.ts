import { Directive, Output, EventEmitter, HostListener } from '@angular/core';

@Directive({
  selector: '[number-format]' // Attribute selector
})
export class NumberFormatDirective {

  constructor() {
    console.log('Hello NumberFormatDirective Directive');
  }

  @Output() ngModelChange: EventEmitter<any> = new EventEmitter();
  value: any;

  @HostListener('input', ['$event']) onInputChange($event) {
    this.value = $event.target.value.replace(/[\.]/g, '').replace(/\B(?=(\d{3})+(?!\d))/g, ".");
    this.ngModelChange.emit(this.value);
  }

}
